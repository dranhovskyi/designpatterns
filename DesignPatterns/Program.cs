﻿using System;
using DesignPatterns.Behavioural;
using DesignPatterns.Behavioural.State;
using DesignPatterns.Creational;
using DesignPatterns.Structural;

namespace DesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            // Creational patterns
            AbstractFactoryDemo.Run();
            //BuilderDemo.Run();
            //FactoryMethodDemo.Run();
            //PrototypeDemo.Run();
            //SingletonDemo.Run();

            // Structural patterns
            //AdapterDemo.Run();
            //BridgeDemo.Run();
            //CompositeDemo.Run();
            //DecoratorDemo.Run();
            //FacadeDemo.Run();
            //FlyweightDemo.Run();
            //ProxyDemo.Run();

            // Behavioural patterns
            //ChainOfResponsibility.Run();
            //CommandDemo.Run();
            //InterpreterDemo.Run();
            //IteratorDemo.Run();
            //MediatorDemo.Run();
            //MementoDemo.Run();
            //ObserverDemo.Run();
            //StateDemo.Run();
            //StrategyDemo.Run();
            //TemplateMethodDemo.Run();
            //VisitorDemo.Run();

            Console.ReadKey();
        }
    }
}